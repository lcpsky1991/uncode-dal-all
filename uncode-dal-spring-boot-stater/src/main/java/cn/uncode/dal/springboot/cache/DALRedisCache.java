package cn.uncode.dal.springboot.cache;


import cn.uncode.cache.CacheUtils;
import cn.uncode.cache.framework.Level;
import cn.uncode.dal.cache.Cache;

public class DALRedisCache implements Cache {
	
	@Override
	public int getSize(String tableSpace) {
		return CacheUtils.getCache(tableSpace).size(Level.Remote);
	}

	@Override
	public void putObject(String tableSpace, String key, Object value) {
		CacheUtils.getCache(tableSpace).put(key, value, Level.Remote);
	}

	@Override
	public void putObject(String tableSpace, String key, Object value, int seconds) {
		CacheUtils.getCache(tableSpace).put(key, value, seconds, Level.Remote);
		
	}

	@Override
	public Object getObject(String tableSpace, String key) {
		return CacheUtils.getCache(tableSpace).get(key, Level.Remote);
	}

	@Override
	public Object removeObject(String tableSpace, String key) {
		CacheUtils.getCache(tableSpace).remove(key, Level.Remote);
		return null;
	}

	@Override
	public void clear(String tableSpace, String id) {
		CacheUtils.getCache(tableSpace).removeAll(Level.Remote);
		
	}

	
}
