package cn.uncode.dal.springboot.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class ClusterDataSourceProperties extends DataSourceProperties{
	
	private String name;
	private String type;
	private String url;
	private String username;
	private String password;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public DataSource build() {
		BasicDataSource basicDataSource = new BasicDataSource();
		basicDataSource.setUrl(url);
		basicDataSource.setUsername(username);
		basicDataSource.setPassword(password);
		coinfig(basicDataSource);
		return basicDataSource;
	}
	
	

}
