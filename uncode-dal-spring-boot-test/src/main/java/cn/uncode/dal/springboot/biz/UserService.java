package cn.uncode.dal.springboot.biz;

import java.util.List;

import cn.uncode.dal.springboot.dto.User;

/**
 * Created by KevinBlandy on 2017/2/28 15:10
 */
public interface UserService{
	
	User users();
	
	Integer create(User userEntity);
	
	Integer addList(List<User> users);
	
	void update(User userEntity);
	
	User get(int id);
	
	void transaction();
	
	void delete(int id);
}
