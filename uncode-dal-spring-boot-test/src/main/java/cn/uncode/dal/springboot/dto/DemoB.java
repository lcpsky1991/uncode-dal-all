package cn.uncode.dal.springboot.dto;

import java.io.Serializable;

public class DemoB implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	private String name;
	
	private DemoA demoA;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DemoA getDemoA() {
		return demoA;
	}

	public void setDemoA(DemoA demoA) {
		this.demoA = demoA;
	}
	
	

}
