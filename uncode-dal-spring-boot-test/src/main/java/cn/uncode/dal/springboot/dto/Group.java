package cn.uncode.dal.springboot.dto;

import java.io.Serializable;

import cn.uncode.dal.annotation.Field;
import cn.uncode.dal.annotation.Table;

@Table(name = "group")
public class Group implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	
	@Field(name = "fbname")
	private String name;
	
	private Role role;
	
	private DemoB demoB;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public DemoB getDemoB() {
		return demoB;
	}

	public void setDemoB(DemoB demoB) {
		this.demoB = demoB;
	}
	
	
	

}
