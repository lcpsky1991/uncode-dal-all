package cn.uncode.dal.spring.jdbc.template;


import cn.uncode.dal.descriptor.Table;
import cn.uncode.dal.jdbc.template.AbstractTemplate;
import cn.uncode.dal.utils.ColumnWrapperUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

public class SqlTemplate extends  AbstractTemplate{
    
    
    @Override
    protected String buildSingleParamSql(String prefix, String column, String columnTwo, Table model, String keyword){
        StringBuffer sql = new StringBuffer();
        if(StringUtils.isNotEmpty(keyword)){
            sql.append(ColumnWrapperUtils.wrap(column)).append(" ").append(keyword);
        }
        sql.append(" ? ");
        return sql.toString();
    }
    @Override
    protected String buildSingleParamSql(String prefix, String column, Table model, String keyword) {
		return  buildSingleParamSql(prefix, column, null, model, keyword);
	}
    @Override
    protected String buildBetweenParamSql(String prefix, String column, Table model, String keyword){
        StringBuffer sql = new StringBuffer();
        if(StringUtils.isNotEmpty(keyword)){
            sql.append(ColumnWrapperUtils.wrap(column)).append(" ").append(keyword);
        }
        sql.append(" ? and ? ");
        return sql.toString();
    }
    @Override
    protected String buildListParamSql(String prefix, String column, Table model, String keyword){
        StringBuffer sql = new StringBuffer();
        
        if("in".equals(keyword) || "not in".equals(keyword)){
        	@SuppressWarnings("unchecked")
            Collection<Object> values = (Collection<Object>)model.getConditions().get(column);
        	int len = values.size();
        	int step = 0;
        	sql.append(ColumnWrapperUtils.wrap(column)).append(" ").append(keyword).append(" (");
        	for(Object value : values){
                sql.append("?");
                if((step+1) != len){
                    sql.append(", ");
                }
                step++;
            }
        	sql.append(") ");
        }else{
        	sql.append(ColumnWrapperUtils.wrap(column)).append(" ").append(keyword).append(" (?) ");
        }
        
        return sql.toString();
    }
	@Override
	protected String buildBatchParamSql(String prefix, String column, Table model, String keyword) {
		// TODO Auto-generated method stub
		return null;
	}
	
    
    
    
   
}
