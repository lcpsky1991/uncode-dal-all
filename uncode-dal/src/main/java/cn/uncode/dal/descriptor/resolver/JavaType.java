package cn.uncode.dal.descriptor.resolver;

import java.util.HashMap;
import java.util.Map;

public enum JavaType {
    
    BOOLEAN("java.lang.Boolean"),
    CHARACTER("java.lang.Character"),
    DOUBLE("java.lang.Double"),
    FLOAT("java.lang.Float"),
    INTEGER("java.lang.Integer"),
    LONG("java.lang.Long"),
    STRING("java.lang.String"),
    SHORT("java.lang.Short"),
    DATE("java.util.Date"),
    BYTE("java.lang.Byte"),
    OBJECT("java.lang.Object");

    public final String TYPE;
    
    private static Map<String,JavaType> codeLookup = new HashMap<String,JavaType>();

    static {
      for (JavaType type : JavaType.values()) {
        codeLookup.put(type.TYPE, type);
      }
    }

    JavaType(String type) {
      this.TYPE = type;
    }

    public static JavaType forType(String type)  {
      return codeLookup.get(type);
    }
    
    public String value(){
        return TYPE;
    }
    
    /**
     * 判断object是否为基本类型
     * @param object
     * @return
     */
     public static boolean isBaseType(Class<?> clazz) {
         if (clazz.equals(java.lang.Integer.class) ||
        		 clazz.equals(int.class) ||
        		 clazz.equals(java.lang.Byte.class) ||
        		 clazz.equals(java.lang.Long.class) ||
        		 clazz.equals(long.class) ||
        		 clazz.equals(java.lang.Double.class) ||
        		 clazz.equals(double.class) ||
        		 clazz.equals(java.lang.Float.class) ||
        		 clazz.equals(float.class) ||
        		 clazz.equals(java.lang.Character.class) ||
        		 clazz.equals(char.class) ||
        		 clazz.equals(java.lang.String.class) ||
        		 clazz.equals(java.lang.Short.class) ||
        		 clazz.equals(short.class) ||
        		 clazz.equals(boolean.class) ||
        		 clazz.equals(java.util.Date.class) ||
        		 clazz.equals(java.lang.Boolean.class)) {
             return true;
         }
         return false;
     }
}
