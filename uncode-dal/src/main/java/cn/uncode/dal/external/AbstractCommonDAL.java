package cn.uncode.dal.external;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.uncode.dal.core.BaseDAL;
import cn.uncode.dal.core.BaseDTO;
import cn.uncode.dal.criteria.Model;
import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.dal.descriptor.QueryResult;
import cn.uncode.dal.router.TableShardingRouter;
import cn.uncode.dal.utils.IDGenerateUtils;
import cn.uncode.dal.utils.ShardsUtils;

public  class AbstractCommonDAL<T> implements CommonDAL<T> {//abstract
	
	public static final int PAGE_SIZE = 15;
    public final static int MAX_QUERY_NUMBER = 1000;
    public final static String DATA = "data";
    public final static String PAGE = "page";

    @Autowired
    protected BaseDAL baseDAL;
    
    private Class<T> clazz;

    @SuppressWarnings("unchecked")
	private Class<T> getClazz() {
        if (clazz == null) {
        	clazz = (Class<T>) (((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments()[0]);
        }
        return clazz;
    }
    
    
    /**
     * 根据id获得对象
     *
     * @param id
     */
    @Override
    public T getEntityById(Object id) {
        if (null == id) {
            return null;
        }
        QueryResult result = this.baseDAL.selectByPrimaryKey(this.getClazz(), id);
        if (null == result) {
            return null;
        }
        return result.as(this.getClazz());
    }
    
    /**
     * 保存对象
     *
     * @param t
     */
	@Override
    public Object insertEntity(T t) {
		Assert.notNull(t, "Entity is required");
		
		Model model = new Model(t);
		Long id = (Long) ShardsUtils.getPrimaryKeyFromContent(model.getContent());
		//当id没有初始化并且该表没有分表处理
		if(id == null && TableShardingRouter.containsTable(model.getTableName())){
			id = IDGenerateUtils.getId();
			 model.getContent().put(BaseDTO.ID, id);
		}
        saveCreateTime(model);
        saveUpdateTime(model);
		try {
            return this.baseDAL.insert(model);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private void saveCreateTime(Model model) {
		Date createTime = null;
        if(model.getContent().containsKey(BaseDTO.CREATE_AT)){
        	createTime = (Date) model.getContent().get(BaseDTO.CREATE_AT);
        }
        if(createTime == null){
            model.getContent().put(BaseDTO.CREATE_AT, System.currentTimeMillis());
        }
	}
	
	private void saveUpdateTime(Model model) {
		Date updateTime = null;
        if(model.getContent().containsKey(BaseDTO.MODIFY_AT)){
        	updateTime = (Date) model.getContent().get(BaseDTO.MODIFY_AT);
        }
        if(updateTime == null){
            model.getContent().put(BaseDTO.MODIFY_AT, System.currentTimeMillis());
        }
	}

    /**
     * 保存对象
     *
     * @param t
     */
	@Override
    public Object saveEntity(T t) {
		Assert.notNull(t, "Entity is required");
		
		Model model = new Model(t);
		Long id = (Long) ShardsUtils.getPrimaryKeyFromContent(model.getContent());
		//当id没有初始化并且该表没有分表处理
		if(id == null && TableShardingRouter.containsTable(model.getTableName())){
			id = IDGenerateUtils.getId();
			 model.getContent().put(BaseDTO.ID, id);
		}
		if(id == null) {
	        saveCreateTime(model);
	        saveUpdateTime(model);
		}else {
			saveUpdateTime(model);
		}
        Long createtime=null;
        if(model.getContent().containsKey(BaseDTO.CREATE_AT)){
            createtime=(Long) model.getContent().get(BaseDTO.CREATE_AT);
        }
        if(createtime == null || createtime.doubleValue() == 0){
            model.getContent().put(BaseDTO.CREATE_AT, System.currentTimeMillis());
        }
		try {
            return this.baseDAL.save(model);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

    /**
     * 删除对象
     *
     * @param id
     */
    @Override
    public int deleteEntityById(Object id) {
        return this.baseDAL.deleteByPrimaryKey(this.getClazz(), id);
    }

    @Override
    public int updateEntityById(T t) {
    	Model model = new Model(t);
    	saveUpdateTime(model);
        return this.baseDAL.updateByPrimaryKey(model);

    }


    /**
     * 根据条件查询
     *
     * @param map
     * @param fields
     */
    @Override
    public List<T> getListByCriteria(Map<String, Object> map, int limit, String... fields) {
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(getClazz());
        queryCriteria.setLimit(limit);
        if (map != null) {
            //增加排序功能
            Object orderByClause=map.get("orderByClause");
            if(null!=orderByClause){
            	queryCriteria.setOrderByClause(String.valueOf(orderByClause));
            	map.remove("orderByClause");
            }
            //搜索条件
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.asList(getClazz());
    }

    @Override
    public List<T> getListByCriteria(Map<String, Object> map, String... fields) {
        return this.getListByCriteria(map, PAGE_SIZE, fields);
    }
    
    @Override
    public List<T> getListByCriteria(Map<String, Object> criteriaMap){
    	QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(getClazz());
        queryCriteria.setLimit(PAGE_SIZE);
        if (criteriaMap != null) {
            //增加排序功能
            Object orderByClause=criteriaMap.get("orderByClause");
            if(null!=orderByClause){
            	queryCriteria.setOrderByClause(String.valueOf(orderByClause));
            	criteriaMap.remove("orderByClause");
            }
            //搜索条件
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
        return result.asList(getClazz());
    }
    
    @Override
	public List<T> getListByCriteria(QueryCriteria queryCriteria, List<String> fields) {
    	queryCriteria.setTable(getClazz());
    	QueryResult result = null;
        if (fields == null || fields.size() == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
		return result.asList(this.getClazz());
	}
    
    @Override
	public List<T> getListByCriteria(QueryCriteria queryCriteria, String... fields) {
    	queryCriteria.setTable(getClazz());
    	QueryResult result = null;
    	if(fields != null && fields.length > 0){
    		result = this.baseDAL.selectByCriteria(fields, queryCriteria);
    	}else{
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
    	}
    	return result.asList(getClazz());
	}
    

	@Override
	public List<T> getListByCriteria(QueryCriteria queryCriteria) {
		queryCriteria.setTable(getClazz());
    	QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
		return result.asList(this.getClazz());
	}
    

	@Override
	public List<Map<String, Object>> getListByCriteria4Map(QueryCriteria queryCriteria) {
		queryCriteria.setTable(getClazz());
		QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
		return result.getList();
	}

	@Override
	public List<Map<String, Object>> getListByCriteria4Map(QueryCriteria queryCriteria, List<String> fields) {
		queryCriteria.setTable(getClazz());
    	QueryResult result = null;
        if (fields == null || fields.size() == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
		return result.getList();
	}


	@Override
	public List<Map<String, Object>> getListByCriteria4Map(QueryCriteria queryCriteria, String... fields) {
		queryCriteria.setTable(getClazz());
    	QueryResult result = null;
        if (fields != null && fields.length > 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
		return result.getList();
	}


    @Override
    public List<T> getAll() {
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(getClazz());
        queryCriteria.setLimit(1000);
        queryCriteria.setPageSize(1000);
        QueryResult result = null;
        result = this.baseDAL.selectByCriteria(queryCriteria);
        return result.asList(getClazz());
    }
    
	@Override
	public Map<String, Object> getPageByCriteria4Map(QueryCriteria queryCriteria, String... fields) {
		Map<String, Object> result = new HashMap<String, Object>();
		queryCriteria.setTable(getClazz());
		QueryResult queryResult = null;
		if(null == fields || fields.length == 0){
			queryResult = this.baseDAL.selectPageByCriteria(queryCriteria);
		}else{
			queryResult = this.baseDAL.selectPageByCriteria(fields, queryCriteria);
		}
		if(queryResult != null){
			result.put(PAGE,  queryResult.getPage());
			result.put(DATA, queryResult.getList());
		}
		return result;
	}
	

	@Override
	public Map<String, Object> getPageByCriteria4Map(QueryCriteria queryCriteria) {
		Map<String, Object> result = new HashMap<String, Object>();
		queryCriteria.setTable(getClazz());
		QueryResult	queryResult = this.baseDAL.selectPageByCriteria(queryCriteria);
		if(queryResult != null){
			result.put(PAGE,  queryResult.getPage());
			result.put(DATA, queryResult.getList());
		}
		return result;
	}
	
	/**
	 * @Title: getPage
	 * @Description: TODO(获得分页信息)
	 * @return Map    返回类型
	 * @param queryResult：查询结果，pageNum：页码
	 * @author run
	 * @date 2015年4 月9日
	 */
	public static Map<String,Object> getPageInfo(QueryResult queryResult,int pageNum){
		if(null == queryResult){
			return null;
		}
		Map<String,Object> info = new HashMap<String, Object>();
		//获得分页信息
		//{recordTotal=13, pageCount=2, pageSize=10, pageIndex=1}
		Map<String, Object>  page = queryResult.getPage();
		if(MapUtils.isEmpty(page)) {
            return null;
        }
		//总条数
		int recordTotal = (int) page.get("recordTotal");
		//总页数
		int pageCount = (int) page.get("pageCount");
		//是否有上一页
		Boolean hasPreviousPage = null;
		//是否有下一页
		Boolean hasNextPage = null;
		if(pageNum==1){
			hasPreviousPage = false;
		}
		else{
			hasPreviousPage = true;
			info.put("prePage", pageNum-1);
		}
		if(pageNum+1>pageCount){
			hasNextPage = false;
		}
		else{
			hasNextPage = true;
			info.put("nextPage", pageNum+1);
		}
		int begin = 1;
		int end = pageCount;
		if(pageNum-4>0){
			begin = pageNum-4;
		}
		if(pageCount<8){
			end = pageCount;
		}
		else if(pageNum+3<8){
			end = 8;
		}
		else if(pageNum+3<pageCount){
			end = pageNum+3;
		}
		//当前显示的页码
		List<Integer> navigatepageNums = new ArrayList<Integer>();
		for (int i = begin; i <= end; i++) {
			navigatepageNums.add(i);
		}
		info.put("total", recordTotal);
		info.put("pageNum", pageNum);
		info.put("hasPreviousPage", hasPreviousPage);
		info.put("hasNextPage", hasNextPage);
		info.put("navigatepageNums", navigatepageNums);
		info.put("pages", pageCount);
		return info;
	}

    /**
     * 根据条件查询对象
     *
     * @param fields
     */
    @Override
    public T getEntityByCriteria(Map<String, Object> criteriaMap, String... fields) {
        QueryCriteria queryCriteria = new QueryCriteria();
        if (criteriaMap != null) {
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        queryCriteria.setSelectOne(true);
        queryCriteria.setTable(getClazz());
        queryCriteria.setDescOrderByClause(BaseDTO.ID);
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.as(getClazz());
    }
    
	@Override
	public T getEntityByCriteria(Map<String, Object> criteriaMap) {
		QueryCriteria queryCriteria = new QueryCriteria();
        if (criteriaMap != null) {
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        queryCriteria.setSelectOne(true);
        queryCriteria.setTable(getClazz());
        queryCriteria.setDescOrderByClause(BaseDTO.ID);
        QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
        return result.as(getClazz());
	}
	
	
	/**
     * 根据条件查询对象
     *
     * @param fields
     */
    @Override
    public T getEntityByCriteria(QueryCriteria queryCriteria, String... fields) {
        queryCriteria.setSelectOne(true);
        queryCriteria.setTable(getClazz());
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.as(getClazz());
    }
    
	@Override
	public T getEntityByCriteria(QueryCriteria queryCriteria) {
        queryCriteria.setSelectOne(true);
        queryCriteria.setTable(getClazz());
        QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
        return result.as(getClazz());
	}

    
    
    @Override
    public Map<String, Object> getEntityByCriteria4Map(Map<String, Object> criteriaMap, String... fields) {
    	QueryCriteria queryCriteria = new QueryCriteria();
    	if (criteriaMap != null) {
    		QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
    		for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
    			criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
    		}
    	}
    	queryCriteria.setSelectOne(true);
    	queryCriteria.setTable(getClazz());
    	queryCriteria.setDescOrderByClause(BaseDTO.ID);
    	QueryResult result = null;
    	if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
    	return result.get();
    }
    
    @Override
	public Map<String, Object> getEntityByCriteria4Map(QueryCriteria queryCriteria, List<String> fields) {
    	queryCriteria.setSelectOne(true);
    	queryCriteria.setTable(getClazz());
    	queryCriteria.setDescOrderByClause(BaseDTO.ID);
    	QueryResult result = null;
    	if (fields == null || fields.size() == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
    	return result.get();
	}


	@Override
	public Map<String, Object> getEntityByCriteria4Map(QueryCriteria criteria) {
		return getEntityByCriteria4Map(criteria, null);
	}
    

	@Override
	public Map<String, Object> getEntityByCriteria4Map(Map<String, Object> criteriaMap) {
		QueryCriteria queryCriteria = new QueryCriteria();
    	if (criteriaMap != null) {
    		QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
    		for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
    			criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
    		}
    	}
    	queryCriteria.setSelectOne(true);
    	queryCriteria.setTable(getClazz());
    	queryCriteria.setDescOrderByClause(BaseDTO.ID);
    	QueryResult	result = this.baseDAL.selectByCriteria(queryCriteria);
    	return result.get();
	}
    
    
    /**
     * 根据id查询对象的一些字段
     *
     * @param id     对象的id
     * @param fields 要查询的字段
     */
    @Override
    public T getEntityById(Object id, List<String> fields) {
        QueryResult result = this.baseDAL.selectByPrimaryKey(fields, this.getClazz(), id);
        if (null == result) {
            return null;
        }
        return result.as(this.getClazz());
    }

    /**
     * 根据id查询对象的一些字段
     *
     * @param id     对象的id
     * @param fields 要查询的字段
     */
    @Override
    public Map<String, Object> getEntityById4Map(Object id, List<String> fields) {
        QueryResult result = this.baseDAL.selectByPrimaryKey(fields, this.getClazz(), id);
        if (null == result) {
            return null;
        }
        return result.get();
    }

    /**
     * 根据条件查询 map 列表
     *
     * @param fields
     */
    @Override
    public List<Map<String, Object>> getListByCriteria4Map(Map<String, Object> map, int pageSize, String... fields) {
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(getClazz());
        queryCriteria.setDescOrderByClause(BaseDTO.ID);
        queryCriteria.setPageSize(pageSize);
        if (map != null) {
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.getList();
    }


    /**
     * 主要功能:
     *
     * @param inMap
     * @param andMap
     * @param fields
     * @param order 排序字段 类型
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<Map<String,Object>> getListMapByFieldsInByOrder(Map<String, Object> inMap, Map<String, Object> andMap, String order,String... fields){
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(this.getClazz());
        queryCriteria.setPageSize(10000);
        queryCriteria.setAscOrderByClause(order);
        QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
        if (inMap != null && !inMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : inMap.entrySet()) {
                criteria.andColumnIn(entry.getKey(), (List<Object>) entry.getValue());
            }
        }
        if (andMap != null && !andMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : andMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.getList();
    }
    /**
     * 主要功能:
     *
     * @param inMap
     * @param andMap
     * @param fields
     * @param order 排序字段 类型
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<T> getListByFieldsInByOrder(Map<String, Object> inMap, Map<String, Object> andMap, String order,String... fields){
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(this.getClazz());
        queryCriteria.setPageSize(10000);
        queryCriteria.setAscOrderByClause(order);
        QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
        if (inMap != null && !inMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : inMap.entrySet()) {
                criteria.andColumnIn(entry.getKey(), (List<Object>) entry.getValue());
            }
        }
        if (andMap != null && !andMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : andMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
        }
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.asList(this.getClazz());
    }

    @Override
    public T getEntityById(Object id, String... fields) {
        Map<String, Object> map = new HashMap<String, Object>(1);
        map.put(BaseDTO.ID, id);
        return (T)this.getEntityByCriteria(map, fields);

    }


    /**
     * 根据查询条件更新
     *
     * @param t        当前对象
     */
    @Override
    public void updateEntityByCriteria(T t, Map<String, Object> criteriaMap) {
        QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(this.getClazz());
        if (criteriaMap != null) {
            QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
            for (Map.Entry<String, Object> entry : criteriaMap.entrySet()) {
                criteria.andColumnEqualTo(entry.getKey(), entry.getValue());
            }
            if(criteriaMap.containsKey("version")){
            	queryCriteria.setVersion(criteriaMap.get("version"));
            }
        }
        this.baseDAL.updateByCriteria(t, queryCriteria);
    }
    
    @Override
    public int updateEntityByCriteria(T t, QueryCriteria queryCriteria) {
    	queryCriteria.setTable(this.getClazz());
        return this.baseDAL.updateByCriteria(t, queryCriteria);
    }

    @Override
    public BaseDAL getBaseDAL() {
        return this.baseDAL;
    }

    /**
     * 根据一组id查询
     * @param ids
     * @param fields
     * @return
     * @author Tony
     * @date 2016年4月7日下午2:00:03
     */
    @Override
    public List<Map<String ,Object>> getListByIds4Map(List<Long> ids,String...fields){
    	QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(this.getClazz());
        queryCriteria.setPageSize(10000);
        queryCriteria.setDescOrderByClause(BaseDTO.ID);
        QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
        criteria.andColumnIn(BaseDTO.ID, ids);
        QueryResult result = null;
        if (fields == null || fields.length == 0) {
            result = this.baseDAL.selectByCriteria(queryCriteria);
        } else {
            result = this.baseDAL.selectByCriteria(fields, queryCriteria);
        }
        return result.getList();
    }

    @Override
    public List<T> getListByIds(List<Long> ids){
    	if(null == ids) {
            return Collections.emptyList();
        }
    	QueryCriteria queryCriteria = new QueryCriteria();
        queryCriteria.setTable(this.getClazz());
        queryCriteria.setPageSize(10000);
        queryCriteria.setDescOrderByClause(BaseDTO.ID);
        QueryCriteria.Criteria criteria = queryCriteria.createCriteria();
        criteria.andColumnIn(BaseDTO.ID, ids);
        QueryResult result = this.baseDAL.selectByCriteria(queryCriteria);
        return result.asList(clazz);
    }
    /**
     *
     * @param queryCriteria
     * @param fields
     * @return all by criteria
     * @author Tony
     * @date 2017年06月28日 17:36
     */
    @Override
    public List<Map<String, Object>> getAll4Map(QueryCriteria queryCriteria, String... fields) {
        List<Map<String, Object>> list = new ArrayList<>();
        boolean flag = true;
        int i = 0;
        while (flag) {
            queryCriteria.setTable(getClazz());
            queryCriteria.setRecordIndex(i);
            queryCriteria.setLimit(MAX_QUERY_NUMBER);
            QueryResult result;
            if (null != fields && fields.length > 0) {
                result = baseDAL.selectByCriteria(fields, queryCriteria);
            } else {
                result = baseDAL.selectByCriteria(queryCriteria);
            }
            if (null == result || CollectionUtils.isEmpty(result.getList())) {
                flag = false;
            } else {
                list.addAll(result.getList());
                i += MAX_QUERY_NUMBER;
                if (result.getList().size() < MAX_QUERY_NUMBER) {
                    flag = false;
                }
            }
        }
        return list;
    }

    @Override
    public List<T> getAll(QueryCriteria queryCriteria, String... fields) {
        List<T> list = new ArrayList<>();
        boolean flag = true;
        int i = 0;
        while (flag) {
            queryCriteria.setTable(getClazz());
            queryCriteria.setRecordIndex(i);
            queryCriteria.setLimit(MAX_QUERY_NUMBER);
            QueryResult result;
            if (null != fields && fields.length > 0) {
                result = baseDAL.selectByCriteria(fields, queryCriteria);
            } else {
                result = baseDAL.selectByCriteria(queryCriteria);
            }
            List<T> childList = result.asList(getClazz());
            if (CollectionUtils.isEmpty(childList)) {
                flag = false;
            } else {
                list.addAll(childList);
                i += MAX_QUERY_NUMBER;
                if (childList.size() < MAX_QUERY_NUMBER) {
                    flag = false;
                }
            }
        }
        return list;
    }
    @Override
    public int count(QueryCriteria queryCriteria) {
        queryCriteria.setTable(getClazz());
        return this.baseDAL.countByCriteria(queryCriteria);
    }


	@Override
	public int deleteEntityByCriteria(QueryCriteria queryCriteria) {
		queryCriteria.setTable(getClazz());
		return baseDAL.deleteByCriteria(queryCriteria);
	}
}
