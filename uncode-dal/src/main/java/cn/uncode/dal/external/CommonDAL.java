package cn.uncode.dal.external;

import java.util.List;
import java.util.Map;

import cn.uncode.dal.core.BaseDAL;
import cn.uncode.dal.criteria.QueryCriteria;

public interface CommonDAL<T> {
	
	
    //-------------------------
  	// get
  	//-------------------------
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * getEntityById(1, fields);
	 * </pre>
	 * @param id 主键标识
	 * @return 查询结果
	 */
	T getEntityById(Object id);
	
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * List<String> fields = new ArrayList<>();
	 * fields.add(User.ID);
	 * fields.add(User.NAME)
	 * getEntityById(1, fields);
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param id 主键标识
	 * @return 查询结果
	 */
	T getEntityById(Object id, List<String> fields);
	
	/**
	 * 根据主键查询单条记录
	 * <pre>
	 * getEntityById(1, new String[]{"id", "name"});
	 * </pre>
	 * @param fields 需要显示的字段集合
	 * @param id 主键标识
	 * @return 查询结果
	 */
	T getEntityById(Object id, String... fields);
	
	/**
	 * 根据条件查询对象
	 * @param criteria
	 * @param fields
	 */
	T getEntityByCriteria(Map<String, Object> criteria, String... fields);
	
	T getEntityByCriteria(Map<String, Object> criteria);
	
	T getEntityByCriteria(QueryCriteria queryCriteria);
	
	T getEntityByCriteria(QueryCriteria queryCriteria, String... fields);
	
	/**
	 * 根据id查询对象的一些字段
	 * @param id   对象的id
	 * @param fields  要查询的字段
	 */
	Map<String,Object> getEntityById4Map(Object id, List<String> fields);
	
	/** 根据条件查询对象
	 * @param criteria
	 * @param fields
	 */
	Map<String, Object> getEntityByCriteria4Map(Map<String, Object> criteria, String ... fields);
	
	
	Map<String, Object> getEntityByCriteria4Map(Map<String, Object> criteria);
	
	
	Map<String, Object> getEntityByCriteria4Map(QueryCriteria criteria, List<String> fields);
	
	Map<String, Object> getEntityByCriteria4Map(QueryCriteria criteria);
	
	
    //-------------------------
  	// select
  	//-------------------------
	/**
     * 根据一组id查询
     * @param ids
     * @param fields
     */
    public List<Map<String,Object>> getListByIds4Map(List<Long> ids, String...fields);

	/**
     * 列表查询
     * @param ids
     */
    public List<T> getListByIds(List<Long> ids);
    
    /**
	 * 更具条件查询
	 * @param criteria 查询条件
	 * @param fields 显示字段
	 */
	List<T> getListByCriteria(Map<String, Object> criteria, String ... fields);
	
	List<T> getListByCriteria(Map<String, Object> criteria);
	
	/**
	 * 根据条件查询 map 列表
	 * @param criteria
	 * @param fields
	 */
	List<T> getListByCriteria(Map<String, Object> criteria, int pageSize, String ... fields);
	/**
	 * 根据条件查询 map 列表
	 * @param criteria
	 * @param fields
	 */
	List<Map<String,Object>> getListByCriteria4Map(Map<String, Object> criteria, int limit, String ... fields);
	
	
	Map<String, Object> getPageByCriteria4Map(QueryCriteria queryCriteria, String ... fields);

	Map<String, Object> getPageByCriteria4Map(QueryCriteria queryCriteria);
	
	List<T> getAll();
	
	List<Map<String,Object>> getListByCriteria4Map(QueryCriteria queryCriteria);
	
	List<T> getListByCriteria(QueryCriteria queryCriteria);
	
	List<T> getListByCriteria(QueryCriteria queryCriteria, List<String> fields);
	
	List<T> getListByCriteria(QueryCriteria queryCriteria, String... fields);
	
	List<Map<String,Object>> getListByCriteria4Map(QueryCriteria queryCriteria, List<String> fields);
	
	List<Map<String,Object>> getListByCriteria4Map(QueryCriteria queryCriteria, String... fields);

	/**
     * 主要功能:
     * @param inMap
     * @param andMap
     * @param fields
     * @param order 排序字段 类型
     */
	public List<Map<String,Object>> getListMapByFieldsInByOrder(Map<String, Object> inMap, Map<String, Object> andMap,String order,String... fields);
	/**
     * 主要功能:
     * @param inMap
     * @param andMap
     * @param fields
     * @param order 排序字段 类型
     */
	public List<T> getListByFieldsInByOrder(Map<String, Object> inMap, Map<String, Object> andMap,String order,String... fields);
	
	
    //-------------------------
  	// save
  	//-------------------------
	/**
	 * 保存对象
	 */
	Object saveEntity(T t);
    //-------------------------
  	// insert
  	//-------------------------
	/**
	 * 保存对象
	 */
	Object insertEntity(T t);
	
    //-------------------------
  	// delete
  	//-------------------------
	/**
	 * 删除对象
	 * @return
	 */
	int deleteEntityById(Object id);
	
	int deleteEntityByCriteria(QueryCriteria queryCriteria);

    //-------------------------
  	// udpate
  	//-------------------------
	/**
	 * 修改
	 */
	int updateEntityById(T t);
	
	/**
	 * 根据查询条件更新
	 * @param t  当前对象
	 * @param criteria  查询条件
	 */
	void updateEntityByCriteria(T t, Map<String, Object> criteria);
	
	/**
	 * 根据查询条件更新
	 * @param t  当前对象
	 * @param queryCriteria  查询条件
	 */
	int updateEntityByCriteria(T t, QueryCriteria queryCriteria);
	

    //-------------------------
  	// other
  	//-------------------------
	public BaseDAL getBaseDAL();


	/**
	 * 根据条件查询所有的数据，没有条数限制.
	 *
	 * @param queryCriteria
	 * @param fields
	 * @return list by criteria
	 * @author Tony
	 * @date 2017年05月19日 10:33
	 */
	List<Map<String, Object>> getAll4Map(QueryCriteria queryCriteria, String... fields);

	/**
	 * 根据条件查询所有的数据对象，没有条数限制.
	 * @param queryCriteria
	 * @param fields
	 * @return
	 * 2017年8月18日
	 * @author jason.li
	 */
	List<T> getAll(QueryCriteria queryCriteria, String... fields);

	/**
	 * 根据条件统计
	 *
	 * @param criteria
	 * @return int
	 * @author Tony
	 * @date 2017年05月25日 19:41
	 */
	int count(QueryCriteria criteria);

}
