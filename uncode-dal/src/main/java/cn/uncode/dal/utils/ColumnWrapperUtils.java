package cn.uncode.dal.utils;

public class ColumnWrapperUtils {

    private static final String KEY_WORDS = "index,key,value,table,option,fields,version,user,name,status,desc,group,signal,order,";

    public static String wrap(String column) {
        if (KEY_WORDS.indexOf(column) != -1) {
            return "`" + column + "`";
        } else {
            return column;
        }
    }
    
    public static String wrapOrder(String column) {
    	StringBuffer sb = new StringBuffer();
    	String[] c = column.split("\\s+");
    	String[] fd = c[0].split(",");
    	for (String f : fd){
    		if (KEY_WORDS.indexOf(f) != -1) {
    			sb.append("`" + f + "`").append(",");
            }else {
            	sb.append(f).append(",");
            }
        }
    	if(sb.lastIndexOf(",") != -1) {
    		sb.deleteCharAt(sb.lastIndexOf(","));
    	}
    	sb.append(" ").append(c[1]);
    	return sb.toString();
    }

}

